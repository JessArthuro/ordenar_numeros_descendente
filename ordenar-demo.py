#creacion de un array
data = []
#lectura del archivo txt
with open('numeros.txt','r') as myfile:
    #lectura del archivo linea por linea
    for line in myfile:
        #el array dara una separacion a la cadena creada
        data.extend(map(int, line.split()))
#imprime el array de forma descendente
print sorted(data, reverse=True) 

